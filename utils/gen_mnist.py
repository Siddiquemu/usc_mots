import os
import sys
coderoot = os.path.dirname(os.path.realpath(__file__)).split('utils')[0]
sys.path.insert(0, coderoot)
import os.path as path
import argparse
import subprocess
from joblib import Parallel, delayed
import multiprocessing
import math
import numpy as np
import torch

#sys.path.append("modules")
import utils_TBA as utils
import matplotlib.pyplot as plt
import torchvision as torchvision
import cv2
import matplotlib.cm as cm
from PIL import Image, ImageDraw
import random
from utils_data import resize_image, crop_mask

parser = argparse.ArgumentParser()
parser.add_argument('--v', type=int, default=0)
parser.add_argument('--metric', type=int, default=1)
parser.add_argument('--max_digits', type=int, default=3)
parser.add_argument('--data_type', type=str, default='train')
args = parser.parse_args()

codeBase = coderoot
metric = args.metric
v = args.v


scale_change = False
scale_change_intrvl = -1  # not used in current cvpr version
shape_change = False
shape_change_intrvl = -1  # not used in current cvpr version

N = 1 if metric == 1 else 64
T = 20
H = 128
W = 128
D = 1
h = 28
w = 28
O = args.max_digits
frame_num = 1e4 if metric == 1 else 2e6  # 1e4
train_ratio = 0 if metric == 1 else 0.96
birth_prob = 0.5
appear_interval = 5

scale_var = 0  # 0.5
shape_var = 0  # 0.2 # not used in current cvpr version
ratio_var = 0  # 0.1
velocity = 5.3
task = 'mnist'
m = h // 3
eps = 1e-5

txt_name = 'gt.txt'
metric_dir = 'metric' if metric == 1 else ''
data_dir = path.join('data', task)
input_dir = path.join(data_dir, 'MNIST', 'processed')
output_dir = path.join(data_dir, 'pt', metric_dir)
output_input_dir = path.join(output_dir, 'input')
utils.rmdir(output_input_dir);
utils.mkdir(output_input_dir)
output_gt_dir = path.join(output_dir, 'gt')

# mnist data
if not path.exists(input_dir):
    import torchvision.datasets as datasets

    datasets.MNIST(root=data_dir, train=True, download=True)
# import mnist labels
import tensorflow as tf

(x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()

train_data = torch.load(path.join(input_dir, 'training.pt'))  # 60000 * 28 * 28
test_data = torch.load(path.join(input_dir, 'test.pt'))  # 10000 * 28 * 28
data = torch.cat((train_data[0], test_data[0]), 0).unsqueeze(3)
labels = np.append(y_train, y_test, axis=0)  # 70000 * h * w * D
data_num = data.size(0)

# generate data from trackers
train_frame_num = frame_num * train_ratio
test_frame_num = frame_num * (1 - train_ratio)
print('train frame number: ' + str(train_frame_num))
print('test frame number: ' + str(test_frame_num))
batch_nums = {
    'train': math.floor(train_frame_num / (N * T)),
    'test': math.floor(test_frame_num / (N * T))
}

core_num = 1 if metric == 1 else multiprocessing.cpu_count()
oid = 0  # object id
print("Running with " + str(core_num) + " cores.")
if metric == 1:
    utils.mkdir(output_gt_dir)
    file = open(path.join(output_gt_dir, txt_name), "w")


def process_batch(states, batch_id):
    import matplotlib.pyplot as plt1
    global oid
    masks_f = []
    boxs_f = []
    buffer_big = torch.ByteTensor(2, H + 2 * h, W + 2 * w, D).zero_()
    org_seq = torch.ByteTensor(T, H, W, D).zero_()
    # sample all the random variables
    unif = torch.rand(T, O)
    data_id = torch.rand(T, O).mul_(data_num).floor_().long()
    direction_id = torch.rand(T, O).mul_(4).floor_().long()  # [0, 3]
    position_id = torch.rand(T, O, 2).mul_(H - 2 * m).add_(m).floor_().long()  # [m, H-m-1]
    scales = torch.rand(T, O).mul_(2).add_(-1).mul_(scale_var).add_(1)  # [1 - var, 1 + var]
    ratios = torch.rand(T, O).mul_(2).add_(-1).mul_(ratio_var).add_(1).sqrt_()  # [sqrt(1 - var), sqrt(1 + var)]
    for t in range(0, T):
        for o in range(0, O):
            if states[o][0] < appear_interval:  # wait for interval frames
                states[o][0] = states[o][0] + 1
            elif states[o][0] == appear_interval:  # allow birth
                if unif[t][o] < birth_prob:  # birth
                    # shape and appearance

                    data_ind = data_id[t][o]  # .data.cpu().numpy()
                    scale = scales[t][o]
                    ratio = ratios[t][o]
                    h_, w_ = torch.round(h * scale * ratio), torch.round(w * scale / ratio)
                    data_patch = data[data_ind]
                    label_patch = labels[data_ind]
                    # randomly select a data patch shape from full range
                    # resiz data patch to predefined shape
                    # data_patch =  utils.resize_data_patch(data_patch, h_, w_)
                    # data_patch = utils.imresize(data[data_ind], h_, w_).unsqueeze(2)
                    # pose
                    direction = direction_id[t][o]
                    position = position_id[t][o]
                    x1, y1, x2, y2 = None, None, None, None
                    if direction == 0:
                        x1 = position[0]
                        y1 = torch.tensor(m)
                        x2 = position[1]
                        y2 = torch.tensor(H - 1 - m)
                    elif direction == 1:
                        x1 = position[0]
                        y1 = torch.tensor(H - 1 - m)
                        x2 = position[1]
                        y2 = torch.tensor(m)
                    elif direction == 2:
                        x1 = torch.tensor(m)
                        y1 = position[0]
                        x2 = torch.tensor(W - 1 - m)
                        y2 = position[1]
                    else:
                        x1 = torch.tensor(W - 1 - m)
                        y1 = position[0]
                        x2 = torch.tensor(m)
                        y2 = position[1]
                    # initial states
                    theta = math.atan2(y2 - y1, x2 - x1)
                    vx = velocity * math.cos(theta)
                    vy = velocity * math.sin(theta)
                    states[o] = [appear_interval + 1, data_patch, [], x1, y1, vx, vy, 0, oid, data_ind, scale, ratio]
                    oid += 1
            else:  # exists
                data_patch = states[o][1]
                # resize data_patch to predefined size
                # data_patch = utils.resize_data_patch(data_patch, w, h)

                x1, y1, vx, vy = states[o][3], states[o][4], states[o][5], states[o][6]
                step = states[o][7]
                x = torch.round(x1 + step * vx)
                y = torch.round(y1 + step * vy)
                if x < m - eps or x > W - 1 - m + eps or y < m - eps or y > H - 1 - m + eps:  # the object disappears
                    states[o][0] = 0
                else:
                    # resize data_patch to predefined size
                    # data_patch =  utils.resize_data_patch(data_patch, w, h)
                    h_, w_ = data_patch.size(0), data_patch.size(1)
                    if t % shape_change_intrvl == 0:
                        # full range of digit data index
                        data_ind = states[o][9]
                        scale_new = states[o][10]
                        if shape_change:
                            # select new shape of same class
                            data_ind = random.choice(np.where(labels == labels[data_ind])[0])
                        data_patch = data[data_ind]
                        states[o][1] = data_patch
                        if scale_change:
                            # select new width, height based on scale variance
                            h_new, w_new = torch.round(h_ * scale_new), torch.round(w_ * scale_new)
                            h_new = int(h_new.numpy() + 1)
                            w_new = int(w_new.numpy() + 1)
                            data_patch = utils.imresize(data[data_ind], h_new, w_new).unsqueeze(2)
                            print('change scale from {},{} to {},{}'.format(h_, w_, h_new, w_new))
                        h_, w_ = data_patch.size(0), data_patch.size(1)

                    '''
                    if batch_id*T+t+1<=20:
                    print('frame: ',batch_id*T+t+1)
                    plt.imshow(data_patch.cpu().numpy()[:,:,0])
                    plt.pause(2)
                    '''
                    # center and start position for the big image
                    center_x = x + w
                    center_y = y + h
                    top = math.floor(center_y - (h_ - 1) / 2)
                    left = math.floor(center_x - (w_ - 1) / 2)
                    # put the patch on image and collect loc feature
                    img = buffer_big[0].zero_()
                    img.narrow(0, top, h_).narrow(1, left, w_).copy_(data_patch)
                    img = img.narrow(0, h, H).narrow(1, w, W)  # H * W * D

                    ### Save box and mask for DHAE evaluation
                    mask = data_patch.numpy()[:, :, 0]

                    # get digit image of frame size
                    digit_256_img = img.numpy()[:, :, 0]
                    # Binarize image
                    digits_frame = np.array(digit_256_img > 0, dtype=np.uint8)
                    # obtaing bbox in image frame
                    box = np.array(cv2.boundingRect(digits_frame))
                    #print(box)
                    # draw box
                    # canv = tensor2pil(img.reshape(W, H))
                    # canv.convert("RGBA")
                    # draw = ImageDraw.Draw(canv)
                    # draw.rectangle(((box[0], box[1]), (box[0] + box[2], box[1] + box[3])), fill=None,outline=color[labels[i]], width=2)
                    # plt.imshow(img.numpy()[:,:,0])
                    # plt.pause(0.5)
                    # plt.close()
                    # synthesize a new frame
                    img_f = img.float()
                    org_img_f = org_seq[t].float()  # H * W * D
                    syn_image = (org_img_f + img_f).clamp_(max=255)
                    org_seq[t].copy_(syn_image.round().byte())
                    # update the position
                    states[o][7] = states[o][7] + 1
                    # save for metric evaluation
                    if metric == 1:
                        file.write("%d,%d,%.3f,%.3f,%.3f,%.3f,1,-1,-1,-1\n" %
                                   (batch_id * T + t + 1, states[o][8] + 1, left - w + 1, top - h + 1, w_, h_))
                    # save box, mask for DHAE clustering and MOT metric evaluation
                    # crop mask
                    # mask = crop_mask(digit_256_img, box)
                    # make sure that mask is square size
                    # mask = resize_image(mask, box, min_dim=28, max_dim=28, min_scale=1,mode='square')
                    masks_f.append(mask)
                    boxs_f.append([batch_id * T + t + 1, states[o][8] + 1, box[0], box[1], box[2], box[3]])
    print('Total object in batch {} is {}'.format(batch_id, states[o][8] + 1))
    boxs_final = np.array(boxs_f)
    masks_final = np.array(masks_f)
    return org_seq, states, masks_final, boxs_final


states_batch = []
dest = os.path.join(codeBase, f'data/MNIST_MOT/{args.data_type}')
print(f'boxs and masks will be saved to {dest}')
if not os.path.exists(dest):
    os.makedirs(dest)

img_path = dest + '/imgs/'
print(f'images will be saved to {img_path}')
if not os.path.exists(img_path):
    os.makedirs(img_path)

v = 1
boxs_all = []
masks_all = []
for n in range(0, N):
    states_batch.append([])
    for o in range(0, O):
        states_batch[n].append([0])  # the states of the o-th object in the n-th sample
with Parallel(n_jobs=core_num, backend="threading") as parallel:
    for split in ['test']:  # 'train',
        S = batch_nums[split]
        ind = 0
        for s in range(0, S):  # for each batch of sequences
            out_batch = parallel(
                delayed(process_batch)(states_batch[n], s) for n in range(0, N))  # N * 2 * T * H * W * D
            out_batch = list(zip(*out_batch))  # 2 * N * T * H * W * D
            # stack out_batch[2] and out_batch[3] as mask and box batch for DHAE
            print('Boxes: ', out_batch[3][0].shape)
            print('Masks: ', out_batch[2][0].shape)

            masks_all.append(out_batch[2][0])
            boxs_all.append(out_batch[3][0])
            org_seq_batch = torch.stack(out_batch[0], dim=0)  # N * T * H * W * D
            states_batch = out_batch[1]  # N * []
            if v == 1:
                for t in range(0, T):
                    img_128 = org_seq_batch[0, t].numpy()[:, :, 0]
                    # dpi = 200
                    # fig = plt.figure(frameon=False)
                    # fig.set_size_inches(img_128.shape[1] / dpi, img_128.shape[0] / dpi)
                    # ax = plt.Axes(fig, [0., 0., 1., 1.])
                    # ax.axis('off')
                    # fig.add_axes(ax)
                    # ax.imshow(img_128)
                    # fig.savefig(dest + '/imgs/{:04d}.png'.format(ind+1),cmap = cm.gray,dpi=dpi)
                    im_pil = Image.fromarray(img_128.astype(np.uint8))
                    im_pil.convert('RGBA')
                    im_pil.save(img_path + '{:04d}.png'.format(ind + 1))
                    ind += 1

            else:
                org_seq_batch = org_seq_batch.permute(0, 1, 4, 2, 3)  # N * T * D * H * W
                # filename = split + '_' + str(s) + '.pt'
                # torch.save(org_seq_batch, path.join(output_input_dir, filename))
            print(split + ': ' + str(s + 1) + ' / ' + str(S))
        boxs = np.array(boxs_all)
        boxs_list = [b for b in boxs if len(b) > 0]
        boxs_train = np.concatenate(boxs_list)

        masks = np.array(masks_all)
        masks_list = [b for b in masks if len(b) > 0]
        mask_train = np.concatenate(masks_list)

        print('Boxes: ', boxs_train.shape)
        print('Masks: ', mask_train.shape)
        np.save(dest + '/boxs_{}digits'.format(O), boxs_train, allow_pickle=True, fix_imports=True)
        np.save(dest + '/masks_3digits'.format(O), mask_train, allow_pickle=True, fix_imports=True)
if metric == 1:
    file.close()

# save the data configuration
data_config = {
    'task': task,
    'train_batch_num': batch_nums['train'],
    'test_batch_num': batch_nums['test'],
    'N': N,
    'T': T,
    'D': D,
    'H': H,
    'W': W,
    'h': h,
    'w': w,
    'zeta_s': scale_var,
    'zeta_r': [1, ratio_var]
}
utils.save_json(data_config, path.join(output_dir, 'data_config.json'))